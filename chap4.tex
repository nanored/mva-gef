\documentclass[11pt]{article}

\usepackage[french]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand*{\dif}{\mathop{}d\!}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- Chapitre 4
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{IV - Un détour par le contrôle optimal}
	
	\subsection*{\textsc{Exercice} IV.1.}
	
	Supposons $(H_f)$. Alors on a :
	$$ | f(q', u') - f(q, u) | \leqslant | f(q', u') - f(q, u') | + | f(q, u') - f(q, u) | \leqslant \left| \frac{\partial f}{\partial q}(\cdot, u') \right|_\infty |q' - q| + \left| \frac{\partial f}{\partial u}(q, \cdot) \right|_\infty | u' - u | $$
	On utilise alors la constante $K$ donnée par l'hypothèse $(H_f)$ :
	$$ | f(q', u') - f(q, u) | \leqslant K \left( (|u'| + 1) |q' - q| + (|q - b| + 1) |u' - u| \right) $$
	C'est bien ce qu'on voulait :
	\begin{center}
		\fbox{Les conditions $(H_f)$ entraînent la condition (33).}
	\end{center}

	\subsection*{\textsc{Exercice} IV.2.}
	
	\paragraph{(1)}
	Pour la dérivée selon $m$, on a :
	$$ f(m + \delta m, u) = \left( u(x_1 + \delta x_1), \cdots, u(x_k + \delta x_k) \right) $$
	Or $u \in V$ est différentiable par hypothèse d'admissibilité de $V$. On a donc :
	$$ u(x_i + \delta x_i) = u(x_i) + \dif u(x_i) \delta x_i + o(\delta x_i) $$
	On obtient donc :
	$$ f(m + \delta m, u) = f(m, u) + (\dif u(x_1) \delta x_1, \cdots, \dif u(x_k) \delta x_k) + o(\delta m) $$
	Ce qui se réécrit :
	\begin{center}
		\fbox{$\frac{\partial f}{\partial m}(m, u) \delta m = (\dif u(x_1) \delta x_1, \cdots, \dif u(x_k) \delta x_k)$}
	\end{center}
	Puis pour la dérivée selon $u$ on a :
	$$ f(m, u + \delta u) = \left( u(x_1) + \delta u(x_1), \cdots, u(x_k) + \delta u(x_k) \right) = f(m, u) + \left( \delta u(x_1), \cdots, \delta u(x_k) \right) $$
	Ce qui nous donne :
	\begin{center}
		\fbox{$\frac{\partial f}{\partial u}(m, u) \delta u = \left( \delta u(x_1), \cdots, \delta u(x_k) \right)$}
	\end{center}
	
	\paragraph{(2)}
	En prenant la norme $\sup$ pour $\mathscr{B}$ on a :
	$$ \left| \frac{\partial f}{\partial m}(m, u) \right| = \max_{1 \leqslant i \leqslant k} | \dif u(x_i) | \leqslant \sup_{\R^d} | \dif u | \leqslant C | u |_V $$
	Où $C$ est la constante dans la définition d'admissibilité de $V$. On obtient la première condition de $(H_f)$. Regardons maintenant la continuité de cette dérivée partielle. Considérons un second point $(m', u')$ :
	$$ \begin{array}{lll}
	\left| \frac{\partial f}{\partial m}(m, u) - \frac{\partial f}{\partial m}(m', u') \right| & = & \max_{1 \leqslant i \leqslant k} | \dif u(x_i) - \dif u'(x_i') | \\
	& \leqslant & \max_{1 \leqslant i \leqslant k} | \dif u(x_i) - \dif u(x_i') + \dif u(x_i') - \dif u'(x_i') | \\
	& \leqslant & \max_{1 \leqslant i \leqslant k} | \dif u(x_i) - \dif u(x_i') | + \sup_{\R^d} | \dif(u - u') | \\
	& \leqslant & \max_{1 \leqslant i \leqslant k} | \dif u(x_i) - \dif u(x_i') | + C | u - u' |_V
	\end{array} $$
	Or $u$ est $\mathscr{C}^1$ donc $\dif u$ est continue, ce qui nous permet de passer à la limite :
	$$ \left| \frac{\partial f}{\partial m}(m, u) - \frac{\partial f}{\partial m}(m', u') \right| \xrightarrow{(m', u') \rightarrow (m, u)} 0 $$
	Finalement $\frac{\partial f}{\partial m}$ est bien continue. \\
	Pour la seconde dérivée partielle, on a :
	$$ \left| \frac{\partial f}{\partial u}(m, u) \delta u \right| = \max_{1 \leqslant i \leqslant k} | \delta u(x_i) |\leqslant \sup_{\R^d} | \delta u | \leqslant C | \delta u |_V $$
	On obtient alors la seconde condition de $(H_f)$ avec l'inégalité :
	$$ \left| \frac{\partial f}{\partial u} \right| \leqslant C $$
	Ensuite il nous faut montrer la continuité de cette dérivée partielle. On commence par remarquer qu'elle ne dépend pas de $u$ mais que de $m$. Il suffit alors de montrer la continuité en $m$. Soit alors un second landmark $m'$. On a :
	$$ \left| \left( \frac{\partial f}{\partial u}(m) - \frac{\partial f}{\partial u}(m') \right) \delta u \right| = \max_{1 \leqslant i \leqslant k} \left| \delta u(x_i) - \delta u (x_i') \right| \leqslant \max_{1 \leqslant i \leqslant k} \left( \sup_{\R^d} | \dif \left( \delta u \right) | \right) \left| x_i - x_i' \right| \leqslant C | \delta u |_V \left| m - m' \right| $$
	Ce qui nous donne :
	$$ \left| \frac{\partial f}{\partial u}(m) - \frac{\partial f}{\partial u}(m') \right| \leqslant C \left| m - m' \right| $$
	Ainsi cette dérivée partielle est Lipschitzienne, ce qui entraîne la continuité de  $\frac{\partial f}{\partial u}$. On peut alors conclure :
	\begin{center}
		\fbox{$f$ est $\mathscr{C}^1$ et satisfait les conditions $(H_f)$.}
	\end{center}
		
	\subsection*{\textsc{Exercice} IV.3.}
	
	\paragraph{(1)}
	Commençons par différencier $f$ :
	$$ f(q + \delta q, u) - f(q, u) = u \circ \left( q + \delta q \right) - u \circ q = \left( \dif u \circ q \right) \cdot \delta q + o(\delta q) $$
	$$ f(q, u + \delta u) - f(q, u) = \delta u \circ q $$
	On obtient les dérivées partielles suivantes :
	\begin{center}
		\fbox{$ \displaystyle \frac{\partial f}{\partial q}(q, u) = \dif u \circ q \qquad \frac{\partial f}{\partial u}(q, u) \delta u = \delta u \circ q $}
	\end{center}
	Regardons maintenant la continuité de la dérivée partielle selon $q$. Considérons un second point $(q', u')$ :
	$$ \begin{array}{lll}
	\displaystyle \left| \frac{\partial f}{\partial q}(q, u) - \frac{\partial f}{\partial q}(q', u') \right|
	& = & \displaystyle \sup_{\delta q \in \mathscr{B}} \frac{|(\dif u \circ q - \dif u' \circ q') \cdot \delta q|}{| \delta q |} \\
	& = & \displaystyle \sup_{\delta q \in \mathscr{B}} \frac{\sup_{x \in U} |(\dif u(q(x)) - \dif u'(q'(x))) \cdot \delta q(x) |}{\sup_{x \in U} | \delta q(x) |} \\
	& \leqslant & \displaystyle \sup_{y \in \R^d} \frac{\sup_{x \in U} |(\dif u(q(x)) - \dif u(q'(x)) + \dif u(q'(x)) - \dif u'(q'(x))) \cdot y |}{|y|} \\
	& \leqslant & \displaystyle \sup_{x \in U} |\dif u(q(x)) - \dif u(q'(x))| + |\dif u(q'(x)) - \dif u'(q'(x))| \\
	& \leqslant & \displaystyle \sup_{x \in U} |\dif u(q(x)) - \dif u(q'(x))| + \sup_{z \in \R^d} |\dif (u - u')(z)| \\
	& \leqslant & \displaystyle \sup_{x \in U} |\dif u(q(x)) - \dif u(q'(x))| + C |u - u'|_V \\
	\end{array} $$
	{\color{red} Cette dernière inégalité ne me permet pas d'aboutir ...} \\
	Ensuite il nous faut montrer la continuité la dérivée partielle selon $u$. On commence par remarquer qu'elle ne dépend pas de $u$ mais que de $q$. Il suffit alors de montrer la continuité en $q$. Soit alors une seconde surface paramétrée $q'$. On a :
	$$ \left| \left( \frac{\partial f}{\partial u}(q) - \frac{\partial f}{\partial u}(q') \right) \delta u \right| = \sup_{x \in U} \left| \delta u(q(x)) - \delta u(q'(x)) \right| \leqslant \sup_{x \in U} \left( \sup_{\R^d} | \dif \left( \delta u \right) | \right) \left| q(x) - q'(x) \right| \leqslant C | \delta u |_V \left| q - q' \right| $$
	Ce qui nous donne :
	$$ \left| \frac{\partial f}{\partial u}(q) - \frac{\partial f}{\partial u}(q') \right| \leqslant C \left| q - q' \right| $$
	Ainsi cette dérivée partielle est Lipschitzienne, ce qui entraîne la continuité de $\frac{\partial f}{\partial u}$.
	\begin{center}
		\fbox{$\frac{\partial f}{\partial u}$ est continue.}
	\end{center}

	\paragraph{(2)}
	On commence par montrer la première condition de $(H_f)$ via :
	$$ \left| \frac{\partial f}{\partial q}(q, u) \right| = \sup_{x \in U} | \dif u(q(x)) | \leqslant \sup_{\R^d} | \dif u | \leqslant C | u |_V $$
	Pour la seconde condition, on a :
	$$ \left| \frac{\partial f}{\partial u}(q, u) \delta u \right| = \sup_{x \in U} | \delta u(q(x)) | \leqslant \sup_{\R^d} | \delta u | \leqslant C | \delta u |_V $$
	Ce qui donne finalement l'inégalité :
	$$ \left| \frac{\partial f}{\partial u} \right| \leqslant C $$
	On peut alors conclure :
	\begin{center}
		\fbox{$f$ satisfait les conditions $(H_f)$.}
	\end{center}
	
	\subsection*{\textsc{Exercice} IV.4.}
	
	$C$ est clairement continue et différentiable, avec :
	$$ \frac{\partial C}{\partial q} = 0 \qquad \frac{\partial C}{\partial u}(q, u) \delta u = \langle u, \delta u \rangle_V $$
	La première dérivée partielle vérifie la seconde condition de $(H_C)$. La seconde dérivée vérifie la première condition de $(H_C)$, puisque l'on a :
	$$ \left| \frac{\partial C}{\partial u} \right| \leqslant | u |_V $$
	\begin{center}
		\fbox{$C$ satisfait les conditions $(H_C)$.}
	\end{center}
	
	
	\subsection*{\textsc{Exercice} IV.5.}
	
	\paragraph{IV.2}
	Voici l'Hamiltonien :
	$$ H(m, p, u) = \left( p \mid f(m, u) \right) - C(m, u) = p(u(x_1), \cdots, u(x_k)) - \frac{1}{2} | u |_V $$
	Puis on obtient les équations :
	$$ \syst{ccl}{
		\displaystyle \dot{q} & = & \displaystyle \frac{\partial H}{\partial p} = (u(x_1), \cdots, u(x_k)) \\[3mm]
		\displaystyle \left( \dot{p}, \delta m \right) & = & \displaystyle - \frac{\partial H}{\partial m} \delta m = - p \left( \dif u(x_1) \delta x_1, \cdots, \dif u(x_k) \delta x_k \right) \\[3mm]
		\displaystyle 0 & = & \displaystyle \frac{\partial H}{\partial u} \delta u = p(\delta u(x_1), \cdots, \delta u(x_k)) - \langle u, \delta u \rangle_V
	} $$
	On note $p_i \in \left( \R^d \right)^*$ tel que $p(m) = \sum_{i = 1}^k p_i(x_i)$. On a alors :
	$$ p(\delta u(x_1), \cdots, \delta u(x_k)) = \sum_{i = 1}^k \left( p_i \mid \delta u(x_i) \right) = \sum_{i = 1}^k \delta_{x_i}^{p_i^*}(\delta u) = \sum_{i = 1}^k \left\langle K \delta_{x_i}^{p_i^*}, \delta u \right\rangle_V $$
	Où $K$ est le noyau de l'ENR $V$. On en déduit que pour satisfaire la dernière équation Hamiltonienne, $u$ doit vérifier :
	\begin{center}
		\fbox{$ \displaystyle u = \sum_{i = 1}^k K(\cdot, x_i) p_i^* $}
	\end{center}

	\paragraph{IV.2}
	Voici l'Hamiltonien :
	$$ H(q, p, u) = \left( p \mid f(q, u) \right) - C(q, u) = p(u \circ q) - \frac{1}{2} | u |_V $$
	Puis on obtient les équations :
	$$ \syst{ccl}{
	\displaystyle \dot{q} & = & \displaystyle \frac{\partial H}{\partial p} = u \circ q \\[3mm]
	\displaystyle \left( \dot{p}, \delta q \right) & = & \displaystyle - \frac{\partial H}{\partial q} \delta q = - \left( p \mid (\dif u \circ q) \cdot \delta q \right) \\[3mm]
	\displaystyle 0 & = & \displaystyle \frac{\partial H}{\partial u} \delta u = \left( p \mid \delta u \circ q \right) - \langle u, \delta u \rangle_V
	} $$
	On note $p(x) \in \left( \R^d \right)^*$ tel que $(p \mid q) = \int_{x \in U} \left( p(x) \mid q(x) \right) dx$. On a alors :
	$$ p \circ \delta u \circ q = \int_{x \in U} \left( p(x) \mid \delta u(q(x)) \right) dx = \int_{x \in U} \delta_{q(x)}^{p(x)^*}(\delta u) = \int_{x \in U} \left\langle K \delta_{q(x)}^{p(x)^*}, \delta u \right\rangle_V $$
	Où $K$ est le noyau de l'ENR $V$. On en déduit que pour satisfaire la dernière équation Hamiltonienne, $u$ doit vérifier :
	\begin{center}
	\fbox{$ \displaystyle u = \int_{x \in U} K(\cdot, q(x)) p(x)^* $}
	\end{center}

\end{document}