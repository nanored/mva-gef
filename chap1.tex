\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- Chapitre 1
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{I - Une petite introduction sur les espaces de formes}
	
	\subsection*{\textsc{Exercice} I.1.}
	
	On pose $f$ la fonction polynomiale de $U = \R^{d+1} \setminus \{ 0 \}$ dans $\R$:
	$$ f(x) = \sum_{i=0}^d x_i^2 \; - \; 1 $$
	On remarque d'abord que $U$ est bien un ouvert de $\R^{d+1}$. De plus $f$ étant polynomiale c'est une fonction de classe $\mathscr{C}^\infty$. On écrit le gradient de cette fonction :
	$$ \nabla f (x) = 2 x $$
	Le gradient de $f$ est non nul sur $U = \R^{d+1} \setminus \{ 0 \}$. Ainsi $f$ est une submersion.
	Finalement, comme $0$ n'appartient pas à la sphère $S^d$, on a :
	$$ S^d = S^d \cap U = f^{-1}(0) $$
	Comme $S^d \subset U$, pour tout point $x$ de $S^d$, $U$ est un ouvert qui contient $x$ et $f$ est une submersion de classe $\mathscr{C}^\infty$ telle que $U \cap S^d = f^{-1}(0)$.
	\begin{center}
		\fbox{Ainsi, $S^d$ est bien une sous-variété $C^\infty$ de dimension $d$ de $\R^{d+1}$.}
	\end{center}

	\subsection*{\textsc{Exercice} I.2.}
	
	On note $S_n(\R)$ l'ensemble des matrices symétriques. Cet espace est isomorphe à $\R^{n(n+1)/2}$. On pose alors $f$ de $U = \left\{ A \in M_n(\R) \mid \det A > 0 \right\}$ dans $S_n(\R)$, l'application suivante :
	$$ f(A) = A^\trans A - I_n $$
	Premièrement $U$ est l'image réciproque de l'ouvert $]0, +\infty[$, par l'application continue $\det$. Donc $U$ est ouvert. De plus $f$ est polynomiale donc $\mathscr{C}^\infty$. Voici sa différentielle :
	$$ \forall H \in M_n(\R), \quad df(A) \cdot H = A^\trans H + H^\trans A $$
	Or pour $A$ dans $U$, $A$ est inversible. Soit alors $B$ une matrice symétrique. On on a :
	$$ H = \frac{1}{2} \left( A^{-1} \right)^\trans B \qquad \Rightarrow \qquad df(A) \cdot H = A^\trans H + H^\trans A = \frac{1}{2} B + \frac{1}{2} B^\trans = B $$
	Ainsi la différentielle de $f$ est surjective en tout point. Donc $f$ est une submersion. Enfin, si $A \in U$ a pour image 0, alors $A^\trans A = I_n$. D'où $(\det A)^2 = 1$. Comme $A$ est dans $U$, on a $\det A > 0$ et ainsi $\det A = 1$. Ainsi :
	$$ f^{-1}(0) = U \cap SO(n) $$
	Finalement on a aussi $SO(n) \subset U$. Comme pour l'exercice précédent, cela nous permet de conclure sur le fait que $SO(n)$ est une sous-variété de dimension :
	$$ d = \dim M_n(\R) - \dim S_n(\R) = n^2 - n(n+1)/2 = n(n-1)/2 $$
	\begin{center}
		\fbox{Ainsi, $SO(n)$ est une sous-variété $C^\infty$ de dimension $n(n-1)/2$ de $M_n(\R) \simeq \R^{n^2}$.}
	\end{center}

	\subsection*{\textsc{Exercice} I.3.}
	
	On considère un pôle $e \in \R^{d+1}$. Il existe un indice $i \in \{1, ..., d+1\}$ tel que $|e_i| = 1$ et pour tout $j \neq i$, $e_j = 0$. On considère ensuite un ouvert $\Omega$ de $\R^d$ défini par :
	$$ \Omega = \left] - \frac{1}{\sqrt{d}}, \, \frac{1}{\sqrt{d}} \right[ $$
	Puis on considère l'application de $\Omega$ dans $\R^{d+1}$ :
	$$ g : (x_1, ..., x_d) \mapsto \left( x_1, \, ..., \, x_{i-1}, \, e_i \sqrt{1 - \sum_{i=0}^d x_i^2}, \, x_i, \, ..., \, x_d \right) $$
	Premièrement, $g$ est bien défini car $|x_i| < 1/\sqrt{d}$. Donc $1 - \sum_{i=0}^d x_i^2 > 0$. Secondement $g(x)$ appartient bien à la sphère $S^d$. En effet :
	$$ \| g(x) \|^2 = \sum_{i=0}^d x_i^2 + e_i^2 \left( 1 - \sum_{i=0}^d x_i^2 \right) = 1 $$
	De plus $g(0) = e$ et $g$ est clairement continue et injective. Ainsi $g$ est bijective de $\Omega$ dans $g(\Omega)$, d'inverse continue :
	$$ h : (x_1, ..., x_{d+1}) \in g(\Omega) \mapsto (x_1, ..., x_{i-1}, x_{i+1}, ..., x_{d+1}) \in \Omega $$
	Ainsi $g$ est un homéomorphisme. De plus $dg(0)$ est la matrice identité à laquelle on ajoute une ligne de 0 à la position $i$. Donc $dg(0)$ est injective. Finalement $g$ défini bien une paramétrisation de la sphère $S^d$ au voisinage du pôle $e$.
	
	\subsection*{\textsc{Exercice} I.4.}
	
	Pour $g \in G$, l'application $A_g$ définie bien une bijection puisque $A_{g^{-1}}$ en est l'application inverse. En effet, soit $x \in X$, on a :
	$$ A_g \left( A_{g^{-1}}(x) \right) = A \left( g g^{-1}, x \right) = A(e, x) = x $$
	
	\subsection*{\textsc{Exercice} I.5.}
	
	La relation est réflexive. En effet si $x=y$ alors $Gx = Gy$ donc $x \sim y$. \\
	La relation est symétrique puisque $x \sim y \Leftrightarrow Gx = Gy \Leftrightarrow y \sim x$. \\
	Enfin la relation est transitive car $x \sim y \text{ et } y \sim z \Rightarrow Gx = Gy \text{ et } Gy = Gz \Rightarrow Gx = Gz \Rightarrow x \sim z$
	
	\subsection*{\textsc{Exercice} I.6.}
	
	La moyenne de Fréchet $x_*$ dans un espace euclidien minimise la fonction convexe suivante :
	$$ f(x) = \sum_{i=1}^k \| x - x_i \|^2 $$
	Le minimum est atteint au point d'annulation du gradient :
	$$ \nabla f(x_*) = 0 \Leftrightarrow 2 \sum_{i=1}^k x_* - x_i = 0 \Leftrightarrow x_* = \frac{1}{k} \sum_{i=1}^k x_i $$
	\begin{center}
		\fbox{Dans un espace euclidien, la moyenne de Fréchet correspond à la moyenne usuelle.}
	\end{center}

	\subsection*{\textsc{Exercice} I.7.}
	
	Soit $g$ et $g'$ deux éléments de $g$ tel que $gx_0 = m$ et $g'x_0 = m'$. Comme $d_G$ est équivariante à droite, on obtient en multipliant à droite par $g^{-1}$ :
	$$ d_G(g, g') = d_G(e, g'g^{-1}) = d(e, g'') \qquad \text{avec } g'' = g'g^{-1} $$
	De plus on a :
	$$ g''m = g'g^{-1}m = g'g^{-1}gx_0 = g'x_0 = m' $$
	Ainsi :
	$$ \left\{ d_G(g, g') \mid gx_0 = m, \; g'x_0 = m' \right\} \subseteq \left\{ d_G(e, g'') \mid g''m = m' \right\} $$
	
	Réciproquement, considérons $g''$ tel que $g''m=m'$. Comme $Gx_0 = M$, on peut prendre $g$ tel que $gx_0=m$ et $g'=g''g$ de tel sorte que :
	$$ d_G(e, g'') = d_G(eg, g''g) = d_G(g, g') $$
	Puis on a bien :
	$$ g'x_0 = g''gx_0 = g''m = m' $$
	D'où l'égalité :
	\begin{center}
		\fbox{$ \displaystyle d_M(m, m') = \inf \left\{ d_G(e, g'') \mid g''m = m' \right\} $}
	\end{center}

	\subsection*{\textsc{Exercice} I.8.}
	
	On commence par remarque que $R_{g_1g_2} = R_{g_2} \circ R_{g_1}$. On calcule alors la différentielle d'une composée :
	$$ d(R_{g_1g_2})(h)\xi = dR_{g_2}(R_{g_1}(h))dR_{g_1}(h)\xi $$
	Or $R_{g_1}(h) = hg_1$. D'où :
	\begin{center}
		\fbox{$\displaystyle d(R_{g_1g_2})(h)\xi = dR_{g_2}(hg_1)dR_{g_1}(h)\xi$}
	\end{center}
	
	\subsection*{\textsc{Exercice} I.9.}
	
	Pour $i \in I$ et $j \in J$, l'application $\varphi_i \times \psi_j$ est bien un homéomorphisme d'inverse :
	$$ (\varphi_i \times \psi_j)^{-1}(x, y) = (\varphi_i^{-1}(x), \psi_j^{-1}(y)) $$
	continue puisque $\phi_i$ et $\psi_j$ sont des eux-même des homéomorphisme. De plus pour $k \in I$ et $l \in J$, on a :
	$$ (\varphi_k \times \psi_l) \circ (\varphi_i \times \psi_j)^{-1}(x, y) = (\varphi_k \circ \varphi_i^{-1}(x), \psi_l \circ \psi_j^{-1}(y)) $$
	Comme les $(\varphi_i)_{i \in I}$ et les $(\psi_j)_{j \in J}$ sont des familles qui engendrent des changements de cartes de classe $\mathscr{C}^p$, on a que les applications $\varphi_k \circ \varphi_i^{-1}$ et $\psi_l \circ \psi_j^{-1}$ sont des difféomorphismes de classe $\mathscr{C}^p$ respectivement de $\varphi_i(U_i \cap U_k)$ dans $\varphi_k(U_i \cap U_k)$ et de $\psi_j(V_j \cap V_l)$ dans $\psi_l(V_j \cap V_l)$. \\
	Ainsi $(\varphi_k \times \psi_l) \circ (\varphi_i \times \psi_j)^{-1}$ est un $\mathscr{C}^p$ difféomorphisme de $\varphi_i \times \psi_j((U_i \cap U_k) \times (V_j \cap V_l))$ dans $\varphi_k \times \psi_l((U_i \cap U_k) \times (V_j \cap V_l))$.
	\begin{center}
		\fbox{$(U_i \times V_j, \varphi_i \times \psi_j)_{i,j \in I \times J}$ engendrent bien des applications de changements de cartes $\mathscr{C}^p$.}
	\end{center}

\end{document}