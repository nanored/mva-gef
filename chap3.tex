\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- Chapitre 3
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{III - Groupes $G_V$ et appariements non-linéaires}
	
	\subsection*{\textsc{Exercice} III.1.}

	\paragraph{(1)}
	On suppose que $f$ est une limite p.p. de fonctions Bochner mesurables $(f_n)_{n \in \N}$. On note $M$ un ensemble négligeable tel que pour tout $x$ dans $I \setminus N$, on ait $f_n(x) \xrightarrow[n \rightarrow +\infty]{} f(x)$. De plus pour chaque fonction $f_n$ on dispose d'une suite de fonctions simples $(f_{n,k})_{k \in \N}$ convergeant p.p. vers $f_n$, d'après la définition de la Bochner mesurabilité. On dispose alors d'un ensemble négligeable $N_n$ tel que pour tout $x$ dans $I \setminus N_n$, on ait $f_{n,k}(x) \xrightarrow[k \rightarrow +\infty]{} f_n(x)$. Finalement on pose :
	$$ N = M \cup \left( \bigcup_{n \in \N} N_n \right) $$
	En tant qu'union dénombrable d'ensembles négligeables, $N$ est lui aussi négligeable.\\
	{\color{red} A FINIR}
	
	\paragraph{(2)}
	On rappelle que $I$ est un intervalle borné de $\R$. On pose $\bar{I} = [a, b]$ et pour $n \in \N^*$, on pose :
	$$ f_n = \sum_{i=1}^n \mathbbm{1}_{[a_{i-1}^{n}, a_i^n[} f\left( \frac{1}{2} \left( f(a_{i-1}^{n}) + f(a_i^n) \right) \right) \qquad \text{avec } \; a_i^n = a + \frac{i}{n} (b-a) $$
	$f_n$ est une fonction simple. Soit alors $x \in I \setminus \{b\}$ et $\epsilon > 0$. Comme $f$ est continue on dispose de $\eta > 0$ tel que pour tout $y \in I$, $|y - x| < \eta$ implique $\| f(y) - f(x) \|_{B_1} < \epsilon$. Ainsi pour $n > (b-a)/\eta$ et $i$ tel que $a_{i-1}^n \leqslant x < a_i^n$, on a :
	$$ a_i^n - a_{i-1}^n = (b-a)/n < \eta $$
	D'où :
	$$ \left| x - a_{i-1}^n \right| < \eta \qquad \text{et} \qquad \left| x - a_i^n \right| < \eta $$
	Et donc :
	$$ \| f_n(x) - f(x) \|_{B_1} \leqslant \frac{1}{2} \left\| f(a_{i-1}^n) - f(x) \right\|_{B_1} + \frac{1}{2} \left\| f(a_i^n) - f(x) \right\|_{B_1} \leqslant \frac{1}{2} \epsilon + \frac{1}{2} \epsilon = \epsilon $$
	Ainsi pour tout $x \in I \setminus \{b\}$, $f_n(x)$ converge vers $f(x)$ lorsque $n$ tend vers l'infini. Donc $f$ est limite p.p. de fonctions simples. On conclue donc :
	\begin{center}
		\fbox{Si $f \in \mathscr{C}(I, B_1)$ alors $f$ est Bochner mesurable.}
	\end{center}

	\paragraph{(3)}
	Par définition il existe une suite $f_n$ de fonctions simples convergeant p.p. vers $f$. Soit $N$ négligeable tel que pour tout $x$ dans $I \setminus N$, $f_n(x)$ converge vers $f(x)$ lorsque $n$ tend vers l'infini. Considérons un $x$ dans $I \setminus N$. Par continuité de $g$ on a aussi $g \circ f_n(x)$ qui tend vers $g \circ f(x)$ lorsque $n$ tend vers l'infini. Ceci étant vrai pour tout $x$ dans $I \setminus N$, la suite $(g \circ f_n)_{n \in \N}$ converge p.p. vers $g \circ f$. De plus si $f_n$ s'écrit :
	$$ f_n = \sum_{i=1}^K \mathbbm{1}_{E_i} b_i $$
	Avec $(E_i)$ une famille de mesurables. On note $\mathcal{K} = \{1, ..., K\}$. Pour $J \subseteq \mathcal{K}$, on peut définir l'ensemble mesurable~:
	$$ F_J = \left( \bigcap_{i \in J} E_i \right) \setminus \left( \bigcup_{i \in \mathcal{K} \setminus J} E_i \right) $$
	Ainsi que les valeurs dans $B_1$ :
	$$ c_J = \sum_{i \in J} b_i $$
	Soit alors $x$ dans $I$ et $J = \{ i \mid x \in E_i \}$. On a $x \in F_J$. Considérons $J'$ dans $\mathcal{K}$ différent de $J$. Soit on a $J' \setminus J \neq \emptyset$ et dans ce cas $x \notin \bigcap_{i \in J'} E_i$. Soit on a $J \setminus J' \neq \emptyset$ et dans ce cas $x \in \bigcup_{i \in \mathcal{K} \setminus J'} E_i$. Donc $x$ n'appartient pas à $F_{J'}$. Ainsi l'ensemble des $F_J$ forment une partition de $I$. De plus pour $x \in F_J$, on a $f_n(x) = c_J$ et donc $g \circ f_n(x) = g(c_J)$. D'où :
	$$ f_n = \sum_{J \subseteq \mathcal{K}} \mathbbm{1}_{F_J} c_J \qquad g \circ f_n = \sum_{J \subseteq \mathcal{K}} \mathbbm{1}_{F_J} g(c_J) $$
	Comme $\mathcal{K}$ est fini, $g \circ f_n$ est bien une fonction simple. On en conclue que $g \circ f$ est limite p.p. de fonctions simples.
	\begin{center}
		\fbox{\parbox{0.85\linewidth}{ \centering
			Si $g: B_1 \rightarrow B_2$ est une application continue et $f: I \rightarrow B_1$ est Bochner mesurable alors $g \circ f$ est aussi Bochner mesurable.
		}}
	\end{center}

	\paragraph{(4)}
	Par définition il existe une suite $f_n$ de fonctions simples convergeant p.p. vers $f$. Soit $n \in \N$. Comme montré à la question précédente il existe une partition finie de $I$, $(E_i)$ et des valeurs $(b_i)$ tel que :
	$$ f_n = \sum_{i=1}^{K_n} \mathbbm{1}_{E_i} b_i $$
	On écrit ensuite $\bar{I} = [a, b]$, et on pose $a_j^n = a + j(b-a)/(n+1)$ et $F_{ij} = E_i \cap [a_j^n, a_{j+1}^n[$ si $j < n$ et $F_{in} = E_i \cap [a_n^n, b]$. Ainsi $(F_{ij})_{0 \leqslant j \leqslant n}$ est une partition de $E_i$. Et donc l'ensemble des $F_{ij}$ forme une partition de $I$. On pose alors $c_j^n = (a_j^n + a_{j+1}^n)/2$. Et on construit la nouvelle fonction simple à valeur de $I \times B_1$ suivante :
	$$ g_n = \sum_{1 \leqslant i \leqslant K_n \atop 0 \leqslant j \leqslant n} \mathbbm{1}_{F_{ij}} \cdot (c_j^n, b_i) $$
	Comme $F_{ij} \subseteq E_i$ on a :
	$$ \pi_2 \circ g_n = f_n \qquad \text{et} \qquad \pi_2 \circ g_n \xrightarrow[n \rightarrow +\infty]{p.p.} f $$
	Où $\pi_2$ est la projection selon la seconde coordonnée. De plus pour $t \in I$, $\epsilon > 0$, et $n > (b-a)/\epsilon$, et en prenant $j$ tel que $a_j^n \leqslant t < a_{j+1}^n$. Alors on a :
	$$ a_{j+1}^n - a_j^n = (b-a)/n < \epsilon $$
	Ce qui nous donne :
	$$ \left| t - \pi_1 \circ g_n(t) \right| = |t - c_j^n| < \epsilon $$
	Ainsi on a la convergence :
	$$ \pi_1 \circ g_n \xrightarrow[n \rightarrow +\infty]{} id_I $$
	On en déduit :
	$$ g_n \xrightarrow[n \rightarrow +\infty]{p.p.} id_I \times f $$
	Par continuité de $g$ on obtient :
	$$ g \circ g_n \xrightarrow[n \rightarrow +\infty]{p.p.} g \circ \left( id_I \times f \right) $$
	Où on peut écrire $g \circ g_n$ comme :
	$$ g \circ g_n = \sum_{1 \leqslant i \leqslant K_n \atop 0 \leqslant j \leqslant n} \mathbbm{1}_{F_{ij}} g(c_j^n, b_i) $$
	$g \circ g_n$ est donc une fonction simple, et cette suite converge presque partout vers $g \circ \left( id_I \times f \right)$. On peut alors conclure :
	\begin{center}
		\fbox{\parbox{0.9\linewidth}{ \centering
				Si $g: I \times B_1 \rightarrow B_2$ est une application continue et $f: I \rightarrow B_1$ est Bochner mesurable alors $g \circ (id_I \times f)$ est aussi Bochner mesurable.
		}}
	\end{center}

	\subsection*{\textsc{Exercice} III.2.}
	
	\subsection*{\textsc{Exercice} III.3.}
	
	\paragraph{(1)}
	On a :
	$$ \begin{array}{lll}
		\int_0^1 | v \star w(t) |_V dt & = & \displaystyle \int_0^{1/2} 2 |w(2t)|_V dt \, + \, \int_{1/2}^1 2 | v(2t-1) |_V dt \\
		& = & \displaystyle \int_0^1 |w(u)|_V du \, + \, \int_0^1 |v(u)|_V du \\
		& = & \displaystyle \| w \|_1 + \| v \|_1 < +\infty
	\end{array} $$
	Ainsi $v \star w$ appartient à $L^1([0, 1], V)$ et $\| v \star w \|_1 = \| v \|_1 + \| w \|_1$. On peut alors considérer son image par $\Phi$. Puis pour $t \leqslant 1/2$, on a :
	$$ \Phi_t^{v \star w}(x) = x + \int_0^t (v \star w)_s \circ \Phi_t^{v \star w}(x) ds = x + \int_0^{t} 2w_{2s} \circ \Phi_s^{v \star w}(x) ds = x + \int_0^{2t} w_r \circ \Phi_{r/2}^{v \star w}(x) dr $$
	Ce qui nous donne pour $t \in [0, 1]$ :
	$$ \Phi_{t/2}^{v \star w}(x) = x + \int_0^{t} w_s \circ \Phi_{s/2}^{v \star w}(x) ds = \Phi_t^w(x) $$
	Prenons maintenant $t > 1/2$ :
	$$ \begin{array}{lll}
		\Phi_t^{v \star w}(x) & = & \displaystyle x + \int_0^{1/2} (v \star w)_s \circ \Phi_t^{v \star w}(x) ds + \int_{1/2}^t 2 v_{2s-1} \circ \Phi_s^{v \star w}(x) ds \\
		& = & \displaystyle \Phi_1^v(x) + \int_0^{2t-1} v_r \circ \Phi_{(r+1)/2}^{v \star w}(x) dr \\
	\end{array} $$
	Soit alors $t \in [0, 1]$, on a :
	$$ \Phi_{(t+1)/2}^{v \star w}(x) = \Phi_1^v(x) + \int_0^{t} v_s \circ \Phi_{(s+1)/2}^{v \star w}(x) ds $$
	Cette équation est vérifié par $t \mapsto \Phi_t^v \circ \Phi_1^w(x)$, donc $\Phi_{(t+1)/2}^{v \star w}(x) = \Phi_t^v \circ \Phi_1^w(x)$. Pour $t = 1$, on obtient~:
	$$ \Phi_1^{v \star w}(x) = \Phi_1^v \circ \Phi_1^w(x) $$
	Comme $v \star w$ appartient à $L^1([0, 1], V)$, alors $\Phi_1^{v \star w}$ appartient à $G_V$. On conclue :
	\begin{center}
		\fbox{$G_V$ est stable par composition et $v \mapsto \Phi_1^v$ est un morphisme de $(L^1([0,1], V), \star)$ dans $(G_V, \circ)$.}
	\end{center}

	\paragraph{(2)}
	D'après la proposition III.3, pour $v \in L^1([0, 1], V)$, $\Phi_1^v$ est un homéomorphisme puisque $V$ est admissible dans la définition de $G_V$. On sait que la composition est associative. Il nous reste donc à montrer que l'inverse de $\Phi_1^v$ est bien dans $G_V$. En prenant $\tilde{v}(t, x) = -v(1-t, x)$ comme proposé dans la démonstration de la proposition III.3, on a :
	$$ \Phi_{1-t}^v(x) = \Phi_t^{\tilde{v}} \left( \Phi_1^v(x) \right) $$
	En particulier pour $t = 1$ on obtient :
	$$ Id_V = \Phi_0^v = \Phi_1^{\tilde{v}} \circ \Phi_1^v \qquad \Leftrightarrow \qquad \left( \Phi_1^v \right)^{-1} = \Phi_1^{\tilde{v}} $$
	Ainsi $G_V$ est stable par inversion. On conclue :
	\begin{center}
		\fbox{$G_V$ est un groupe d'homéomorphismes sur $\R^d$.}
	\end{center}

	\subsection*{\textsc{Exercice} III.4.}
	
	\paragraph{}
	Soit $\varphi, \varphi'$ et $\varphi''$ trois éléments de $G_V$. On a :
	$$ d_G(\varphi, \varphi'') = d_G(Id, \varphi'' \circ \varphi^{-1}) = \inf \left\{ \| v \|_1 \bigm| v \in L^1([0, 1], V), \; \Phi_1^v = \varphi'' \circ \varphi^{-1} \right\} $$
	Soit ensuite $(v_n)_{n \in \N}$ et $(w_n)_{n \in \N}$ deux suites dans $V$ tel que pour tout $n$ :
	$$ \Phi_1^{v_n} = \varphi'' \circ \varphi'^{-1} \qquad \text{et} \qquad \Phi_1^{w_n} = \varphi' \circ \varphi^{-1} $$
	et tel que :
	$$ \lim\limits_{n \rightarrow +\infty} \| v_n \|_1 = d_G(\varphi', \varphi'') \qquad \lim\limits_{n \rightarrow +\infty} \| w_n \|_1 = d_G(\varphi, \varphi') $$
	On remarque que :
	$$ \Phi_1^{v_n \star w_n} = \Phi_1^{v_n} \circ \Phi_1^{w_n} = \left( \varphi'' \circ \varphi'^{-1} \right) \circ \left( \varphi' \circ \varphi^{-1} \right) = \varphi'' \circ \varphi^{-1} $$
	Donc pour tout $n$, on a l'inégalité : $d_G(\varphi, \varphi'') \leqslant \| v_n \star w_n \|_1$. Par passage à la limite on obtient :
	$$ d_G(\varphi, \varphi'') \leqslant \lim\limits_{n \rightarrow +\infty} \| v_n \star w_n \|_1 = \lim\limits_{n \rightarrow +\infty} \| v_n \|_1 + \lim\limits_{n \rightarrow +\infty} \| w_n \|_1 = d_G(\varphi', \varphi'') + d_G(\varphi, \varphi') $$
	L'inégalité triangulaire est donc vérifiée.
	
	\paragraph{}
	Soit $\varphi \in G_V$ avec $\varphi \neq Id$. On dispose alors de $x \in \R^d$ tel que $\varphi(x) \neq x$. On note $\epsilon = \| \varphi(x) - x \|_2$. Soit ensuite $v \in L^1([0, 1], V)$ tel que $\varphi = \Phi_1^v$. On a alors :
	$$ \epsilon = \left\| \int_0^1 v_t \circ \Phi_t^v(x) dt \right\|_2 \leqslant \int_0^1 \left\| v_t \circ \Phi_t^v(x) \right\|_2 dt \leqslant \int_0^1 \left\| v_t \right\|_\infty dt \leqslant K \int_0^1 | v_t |_V dt = K \| v \|_1 $$
	Où $K$ est la constante strictement positive dans la définition de l'admissibilité de $V$. On en déduit que $d_G(Id, \varphi) \geqslant \epsilon / K$, et donc $d_G(Id, \varphi) \neq 0$. Par contraposé on obtient :
	$$ d_G(Id, \varphi) = 0 \Rightarrow \varphi = Id $$
	Comme $\Phi_1^0 = Id$, on a $d_G(Id, \varphi) = 0$.
	\begin{center}
		\fbox{L'application $d_G$ vérifié l'inégalité triangulaire et la séparation. C'est donc une distance.}
	\end{center}

	\subsection*{\textsc{Exercice} III.5.}
	
	Pour $v \in V$ et $\gamma \in \mathscr{C}([0, 1], \R^d)$, on pose l'action infinitésimale suivante :
	$$ f(v, \gamma) = v.\gamma = v \circ \gamma $$
	On a alors $B_c = V$ un espace de Banach et $\mathscr{B} = B_e = \mathscr{C}([0, 1], \R^d)$ un autre espace de Banach avec la norme infinie, bien définie puisque l'image continue d'un compact est bornée. On a ensuite :
	$$ | f(v, \gamma) - f(v', \gamma') |_{B_e} = \| v \circ \gamma - v' \circ \gamma' \|_\infty \leqslant \| v - v' \|_\infty + \| dv' \|_\infty \| \gamma - \gamma' \|_\infty $$
	Or par admissibilité de $V$ il existe une constante $K > 0$ tel que :
	$$ \forall v \in V, \quad \| v \|_\infty + \| dv \|_\infty \leqslant K | v |_V $$
	Cela nous permet de conclure :
	$$ | f(v, \gamma) - f(v', \gamma') |_{B_e} \leqslant K \left( | v - v' |_V + | v' |_V | \gamma - \gamma' |_{B_e} \right) $$
	Si on prend $v \in L^2([0, 1], V)$, alors on a aussi $v \in L^1([0, 1], V)$ puisque $I$ est fini. Finalement on satisfait les conditions pour appliquer la proposition III.2 qui nous donne l'existence et l'unicité d'une solution à l'EDO $\dot{\gamma}_t = v_t . \gamma_t$. La formulation intégrale s'écrit :
	\begin{equation}
		\label{int}
		\gamma_t = \gamma_0 + \int_0^t v_s \circ \gamma_s ds
	\end{equation}
	Et donc en posant :
	$$ \Phi_t^v(x) = x + \int_0^t v_s \circ \Phi_s^v(x) ds $$
	On obtient :
	$$ \Phi_t^v \circ \gamma_0(u) = \gamma_0(u) + \int_0^t v_s \circ \Phi_s^v \circ \gamma_0(u) ds $$
	Comme \eqref{int} admet une unique solution on obtient alors l'égalité : $\Phi_t^v . \gamma_0 = \gamma_t$.

\end{document}