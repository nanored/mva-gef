\documentclass[11pt]{article}

\usepackage[french]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\DeclareMathOperator{\EVec}{Vec}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- Chapitre 2
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{II - Métriques locales à noyaux et appariements linéaires de points}
	
	\subsection*{\textsc{Exercice} II.1.}
	
	\subsection*{\textsc{Exercice} II.2.}
	
	\paragraph{}
	Montrons que le noyau $K$ est positif. On a :
	$$ K(y, x) = f(y)^\trans f(x) = \left( f(x)^\trans f(y) \right)^\trans = K(x, y)^\trans $$
	Soit $(x_i)_{1 \leqslant i \leqslant n}$ une famille de $\mathscr{H}$ et $(\alpha_i)_{1 \leqslant i \leqslant n}$ des réels. Alors :
	$$ \sum_{1 \leqslant i,j \leqslant n} \alpha_i K(x_i, x_j) \alpha_j = \sum_{1 \leqslant i,j \leqslant n} \alpha_i f(x_i)^\trans f(x_j) \alpha_j = \left\| \sum_{i=1}^n \alpha_i f(x_i) \right\|^2 \geqslant 0 $$
	Ainsi $K$ est bien un noyau positif.
	
	\paragraph{}
	On sait que l'ensemble $H_f$ des fonctions qui s'écrivent $x \mapsto \sum_{i=1}^n K(x, x_i) \alpha_i$ est dense dans notre ENR $H$. Ainsi pour $y \in \EVec(f(\mathscr{H}))$ la fonction $h_y : x \mapsto f(x)^\trans y$ appartient à $H$. De plus pour $y = \sum_{i=1}^{d} \alpha_i f(y_i)$ et $z = \sum_{j=1}^{d} \beta_j f(z_j)$ :
	$$ \langle h_y, h_z \rangle_H = \sum_{1 \leqslant i,j \leqslant d} \alpha_i f(y_i)^\trans f(z_j) \beta_j = y^\trans z $$
	Finalement $H_f$ est isométrique à $Vec(f(\mathscr{H}))$ qui est un espace de Hilbert. Donc notre ENR $H$ est en fait $H_f$.
	\begin{center}
		\fbox{$ \displaystyle H = \left\{ x \mapsto f(x)^\trans y \mid y \in \EVec \left( f(\mathscr{H}) \right) \right\} $}
	\end{center}
	
	\subsection*{\textsc{Exercice} II.3.}
	
	\paragraph{}
	Soit $A$ et $B$ deux matrices complexes symétriques positives de dimension $d$. On a :
	$$ (A \circ B)_{ij} = A_{ij} B_{ij} = A_{ji} B_{ji} = (A \circ B)_{ji} $$
	D'où la symétrie du produit. Si $A=uu^*$ et $B=vv^*$. On a :
	$$ (A \circ B)_{ij} = A_{ij} B_{ij} = u_i u_j^* v_i v_j^* = (u_i v_i) (u_j v_j)^* = ((u \circ v) (u \circ v)^*)_{ij} $$
	Ainsi en prenant $w \in \C^d$, on a :
	$$ w^* (A \circ B) w = w^* (u \circ v) (u \circ v)^* w = \left| (u \circ v)^* w \right|^2 \geqslant 0 $$
	Dans ce cas, le produit est positif. Plaçons-nous désormais dans le cas général. On sait que $A$ et $B$ sont orthodiagonalisables ce qui nous donne l'existence de deux bases orthonormales de $\C^d$, $(u_i)_{1 \leqslant i \leqslant d}$ et $(v_i)_{1 \leqslant i \leqslant d}$ ainsi que deux vecteurs de valeurs propres $\alpha$ et $\beta$ tel que :
	$$ A = \sum_{i=1}^d \alpha_i u_i u_i^* \qquad B = \sum_{i=1}^d \beta_i v_i v_i^* $$
	On exprime désormais le produit d'Hadamard de ces deux matrices comme suit :
	$$ A \circ B = \sum_{1 \leqslant i,j \leqslant d} \alpha_i \beta_j A_i \circ B_j \qquad \text{avec } A_i = u_i u_i^*, \; B_j = v_j v_j^* $$
	Comme montré précédemment, les matrices $A_i \circ B_j$ sont positives. De plus comme $A$ et $B$ sont positives leurs valeurs propres ($\alpha$ et $\beta$) sont positives. Ainsi $A \circ B$ est une somme pondéré avec des valeurs positives de matrices positives. Donc $A \circ B$ est positive.
	\begin{center}
		\fbox{Si $A$ et $B$ sont symétriques positives alors c'est aussi le cas de $A \circ B$.}
	\end{center}

	\paragraph{}
	Soit $K_1$ et $K_2$ deux noyaux positifs. On pose $K = K_1 \circ K_2$. Soit $x$ et $y$ dans $\mathscr{H}$ et $i,j$ dans $\{1, ..., m\}$. On a~:
	$$ K(x, y)_{ij} = K_1(x, y)_{ij} K_2(x, y)_{ij} = K_1(y, x)_{ji} K_2(y, x)_{ji} = K(y, x)_{ji} $$
	D'où la symétrie :
	$$ K(x, y) = K(y, x)^\trans $$
	Désormais on considère une famille $x_1, ..., x_n$ de $\mathscr{H}$. On pose $\mathbb{K}_1$ la matrice $\left( K_1(x_i, x_j) \right)_{1 \leqslant i, j \leqslant n}$ par blocs. Comme $K_1(x, y) = K_1(y, x)^\trans$, la matrice $\mathbb{K}_1$ est symétrique. De plus pour toute famille $\alpha_1, ..., \alpha_n$ de $\R^m$, en posant $\alpha$ le vecteur colonne qui alignes ces $n$ vecteurs, on a :
	\begin{equation}
		\label{tomat}
		\sum_{1 \leqslant i,j \leqslant n} \alpha_i^\trans K_1(x_i, x_j) \alpha_j = \alpha^\trans \mathbb{K}_1 \alpha
	\end{equation}
	Comme le noyau $K_1$ est positif, on obtient que $\mathbb{K}_1$ est positif. On construit de même $\mathbb{K}_2$ qui est lui aussi symétrique positif. De la même manière on pose $\mathbb{K}$ la matrice par bloc pour le noyau $K$. On remarque que :
	$$ \mathbb{K} = \mathbb{K}_1 \circ \mathbb{K}_2 $$
	D'après le dernier résultat encadré, $\mathbb{K}$ est aussi une matrice symétrique positive. Puis en utilisant l'équation \eqref{tomat} on obtient :
	$$ \sum_{1 \leqslant i,j \leqslant n} \alpha_i^\trans K(x_i, x_j) \alpha_j = \alpha^\trans \mathbb{K} \alpha \geqslant 0 $$
	Ce qui nous permet de conclure :
	\begin{center}
		\fbox{Si $K_1$ et $K_2$ sont deux noyaux positifs alors $K = K_1 \circ K_2$ l'est aussi.}
	\end{center}

	\subsection*{\textsc{Exercice} II.4.}
	
	Supposons que les translations sont des isométries. Soit $x$ et $y$ dans $\mathscr{H}$ et $\alpha, \beta$ dans $\R^m$. On a~:
	$$ \alpha^\trans K(x, y) \beta = \left\langle K \delta_x^\alpha, K \delta_y^\beta \right\rangle_H = \delta_x^\alpha \left( K \delta_y^\beta \right) = \alpha^\trans K \delta_y^\beta(x) $$
	Ce qui nous permet d'exprimer la translation de $K \delta_y^\beta$ :
	$$ \tau K \delta_y^\beta(x) = K(x + \tau, y) \beta $$
	On calcule alors le produit scalaire de cette translation avec $K \delta_x^\alpha$ :
	$$ \left\langle K \delta_x^\alpha, \tau K \delta_y^\beta \right\rangle_H = \alpha^\trans \tau K \delta_y^\beta(x) = \alpha K(x + \tau, y) \beta = \beta^\trans K(y, x + \tau) \alpha = \beta^\trans K \delta_{x+\tau}^\alpha(y) = \left\langle K \delta_{x+\tau}^\alpha, K \delta_y^\beta \right\rangle_H $$
	Comme les translations sont des isométrie, en composant par $-\tau$ on obtient :
	$$ \left\langle K \delta_x^\alpha, \tau K \delta_y^\beta \right\rangle_H = \left\langle (-\tau) K \delta_x^\alpha, K \delta_y^\beta \right\rangle_H = \left\langle K \delta_x^\alpha, K \delta_{y - \tau}^\beta \right\rangle_H $$
	Comme l'espace vectoriel engendré par $\left\{ K \delta_z^\gamma \mid z \in \mathscr{H}, \gamma \in \R^m \right\}$ est dense dans $H$ on identifie alors~:
	$$ \tau K \delta_y^\beta = K \delta_{y - \tau}^\beta $$
	Cela nous donne en utilisant le fait que les translations sont des isométries :
	$$ \alpha^\trans K(x, y) \beta = \left\langle K \delta_x^\alpha, K \delta_y^\beta \right\rangle_H = \left\langle \tau K \delta_x^\alpha, \tau K \delta_y^\beta \right\rangle_H = \left\langle K \delta_{x-\tau}^\alpha, K \delta_{y-\tau}^\beta \right\rangle_H = \alpha^\trans K(x-\tau, y+\tau) \beta $$
	On identifie à nouveau :
	$$ \forall \tau \in \R^m, \; K(x - \tau, y - \tau) = K(x, y) $$
	\begin{center}
		\fbox{On en déduit que si les translations sont des isométries alors $K$ est invariant par translation.}
	\end{center}
	
	\subsection*{\textsc{Exercice} II.5.}
	
	Considérons la mesure définie par une densité par rapport à la mesure de Lebesgue positive et finie sur $\R_+$ :
	$$ d\mu(u) = 2ue^{-u^2} du $$
	On a alors :
	$$ \int_0^{+\infty} \exp(-r^2 u^2) d\mu(u) = \int_0^{+\infty} 2u \exp(-(r^2 + 1) u^2) du = \left[ - \frac{\exp(-(r^2 + 1) u^2)}{r^2 + 1} \right]_0^{+\infty} = \frac{1}{r^2 + 1} = \rho(r) $$
	\begin{center}
		\fbox{Ainsi d'après le théorème de Schoenberg, le noyau $\rho(r)=1/(1+r^2)$ définit un noyau positif.}
	\end{center}
	
	\subsection*{\textsc{Exercice} II.6.}
	
	On comme par remarquer l'égalité suivante :
	\begin{equation}
		\label{nK}
		\| K \delta_x^\alpha - K \delta_y^\alpha \|_H^2 = \langle K \delta_x^\alpha - K \delta_y^\alpha, K \delta_x^\alpha - K \delta_y^\alpha \rangle_H = \alpha^\trans \left( K(x, x) - K(x, y) - K(y, x) + K(y, y) \right) \alpha
	\end{equation}
	Or si $K$ est continue on a :
	$$ K(x, y) \xrightarrow[y \rightarrow x]{} K(x, x) \qquad K(y, x) \xrightarrow[y \rightarrow x]{} K(x, x) \qquad K(y, y) \xrightarrow[y \rightarrow x]{} K(x, x) $$
	D'où :
	$$ K(x, x) - K(x, y) - K(y, x) + K(y, y) \xrightarrow[y \rightarrow x]{} 0_{\mathscr{M}_m(\R)} $$
	Ainsi en reprenant \eqref{nK}, comme $M \mapsto \alpha^\trans M \alpha$ est continue puisque linéaire en dimension fini, on obtient :
	$$ \| K \delta_x^\alpha - K \delta_y^\alpha \|_H^2 \xrightarrow[y \rightarrow x]{} 0 $$
	Ce qui nous montre que :
	\begin{center}
		\fbox{L'application $x \mapsto K \delta_x^\alpha$ est continue si $K$ est continue.}
	\end{center}
	
	\subsection*{\textsc{Exercice} II.7.}
	
	\paragraph{(1)}
	On peux exprimer $\rho$ en fonction de sa transformée de Fourier :
	$$ \rho(x) = \frac{1}{(2\pi)^d} \int \hat{\rho}(\xi) e^{i \langle x, \xi \rangle} d\xi $$
	Cela nous donne :
	$$ \begin{array}{lll}
		\sum_{i,j \in I} \rho(x_i - x_j)c_i c_j & = & \displaystyle \frac{1}{(2\pi)^d} \int \sum_{i,j \in I} \hat{\rho}(\xi) e^{i \langle x_i - x_j, \xi \rangle} c_i c_j d\xi \\
		& = & \displaystyle \frac{1}{(2\pi)^d} \int \sum_{i,j \in I} \overline{c_i e^{- i \langle x_i, \xi \rangle}} c_j e^{- i \langle x_j, \xi \rangle} \hat{\rho}(\xi) d\xi \\
		& = & \displaystyle \frac{1}{(2\pi)^d} \int \left| \sum_{j \in I} c_i e^{- i \langle x_i, \xi \rangle} \right|^2 \hat{\rho}(\xi) d\xi
	\end{array} $$
	
	\paragraph{(2)}
	Soit $c$ un vecteur non nul de $\R^{|I|d}$, avec :
	$$ c = \pmat{c_1^{(1)} & \cdots & c_1^{(d)} & c_2^{(1)} & \cdots & c_2^{(d)} & \cdots & c_{|I|}^{(1)} & \cdots & c_{|I|}^{(d)}}^\trans $$
	Cela nous permet d'écrire :
	$$ c^\trans \mathbb{K} c = \sum_{k=1}^d \sum_{i,j \in I} \rho(x_i - x_j)c_i^{(k)} c_j^{(k)} $$
	Finalement si $\hat{\rho}$ est réel et strictement positif, $c$ est non nul et les $x_i$ sont deux à deux disjoints alors :
	$$ \sum_{i,j \in I} \rho(x_i - x_j)c_i^{(k)} c_j^{(k)} = \frac{1}{(2\pi)^d} \int \left| \sum_{j \in I} c_i^{(k)} e^{- i \langle x_i, \xi \rangle} \right|^2 \hat{\rho}(\xi) d\xi > 0 $$
	Ce qui nous permet de déduire que $\mathbb{K}$ est défini positif et donc inversible.
	
	\paragraph{}
	Dans le cas gaussien on a :
	$$ \rho(x) = e^{-\frac{1}{2 \sigma^2}\|x\|^2} $$
	On calcule sa transformée de Fourier :
	$$ \begin{array}{lll}
		\hat{\rho}(\xi) & = & \displaystyle \int e^{-\frac{1}{2 \sigma^2}\|x\|^2} e^{-i \langle x, \xi \rangle} dx \\
		& = & \displaystyle \prod_{j=1}^d \int e^{-\frac{1}{2 \sigma^2} x_j^2} e^{-i x_j \xi_j} dx_j \\
		& = & \displaystyle \prod_{j=1}^d e^{-\frac{\sigma^2}{2} \xi_j^2} \int e^{-\frac{1}{2 \sigma^2} (x_j + i\sigma^2 \xi_j)^2} dx_j \\
		& = & \displaystyle \prod_{j=1}^d e^{-\frac{\sigma^2}{2} \xi_j^2} \sqrt{2 \pi \sigma^2} \\
		& = & \displaystyle \left( 2 \pi \sigma^2 \right)^{d/2} e^{-\frac{\sigma^2}{2} \| \xi \|^2}  \\
	\end{array} $$
	Ainsi $\hat{\rho}$ est réel et strictement positif.
	\begin{center}
		\fbox{Dans le cas d'un noyau gaussien $\mathbb{K}$ est bien inversible.}
	\end{center}

	\subsection*{\textsc{Exercice} II.8.}

	$J$ est une somme pondéré par des coefficients positifs de normes au carré. Ainsi $J$ est une fonction convexe et il suffit de trouver un appariement $u$ qui annule le gradient de $J$ pour trouver le minimum. Calculons ce gradient :
	$$ \nabla J(u) = u + \gamma^2 \sum_{i \in I} K \left( \delta_{x_i}^{u(x_i) - a_i} \right) $$
	Où $ K \left( \delta_{x_i}^{u(x_i) - a_i} \right) = K(\cdot, x_i) (u(x_i) - a_i)$. Ce qui nous donne :
	\begin{equation}
		\label{grad_vanish}
		u_* = \gamma^2 \sum_{i \in I} K(\cdot, x_i) (a_i - u_*(x_i))
	\end{equation}
	La solution s'écrit donc bien sous la forme proposé avec :
	\begin{equation}
		\label{alpha}
		\alpha_i = \gamma^2 (a_i - u_*(x_i))
	\end{equation}
	Il nous reste plus qu'à trouver le vecteur colonne $u(x_I) \in \R^{|I|d}$. Pour cela on évalue \eqref{grad_vanish} en $x_I$ et on obtient l'équation :
	$$ u_*(x_I) = \gamma^2 \mathbb{K} (a_I - u_*(x_I)) $$
	Ce qui nous donne :
	$$ u_*(x_I) = \left( \mathbb{K} + Id/\gamma^2 \right)^{-1} \mathbb{K} a_I $$
	On peut alors calculer $\alpha_I$ via l'équation \eqref{alpha} :
	$$ \alpha_I = \gamma^2 \left( Id - \left( \mathbb{K} + Id/\gamma^2 \right)^{-1} \mathbb{K} \right) a = \gamma^2 \left( \mathbb{K} + Id/\gamma^2 \right)^{-1} \left( \mathbb{K} + Id/\gamma^2 - \mathbb{K} \right) a = \left( \mathbb{K} + Id/\gamma^2 \right)^{-1} a $$
	Cela nous permet de conclure sur la forme de la solution :
	\begin{center}
		\fbox{$\displaystyle u_* = \sum_{i \in I} K(\cdot, x_i) \alpha_i \qquad \text{avec } \; \alpha_I = \left( \mathbb{K} + Id/\gamma^2 \right)^{-1} a_I$}
	\end{center}
	Maintenant nous pouvons évaluer $J$ en $u_*$ :
	$$ J(u_*) = \frac{1}{2} \| u \|_V^2 + \frac{\gamma^2}{2} \sum_{i \in I} \| u_*(x_i) - a_i \|^2 = \frac{1}{2} \| u \|_V^2 + \frac{\gamma^2}{2} \| u_*(x_I) - a_I \|^2 = \frac{1}{2} \| u \|_V^2 + \frac{1}{2 \gamma^2} \| \alpha_I \|^2 $$
	Où :
	$$ \| u \|_V^2 = \alpha_I^\trans \mathbb{K} \alpha_I \quad \text{ et } \quad  \| \alpha_I \|^2 = \alpha_I^\trans \alpha_I $$
	Ce qui nous permet de conclure :
	\begin{center}
		\fbox{$ \displaystyle J(u_*) = \frac{1}{2} \alpha_I^\trans \left( \mathbb{K} + Id/\gamma^2 \right) \alpha_I $}
	\end{center}
	Lorsque $\gamma$ tend vers l'infini, $\alpha_I$ tend vers $\mathbb{K}^{-1} a_I$.
	
\end{document}