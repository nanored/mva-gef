\documentclass[11pt]{article}

\usepackage[french]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\renewcommand{\d}{\mathrm{d}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- Chapitre 5
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section*{V - Flots géodésiques et équations hamiltoniennes}
	
	\subsection*{\textsc{Exercice} V.1.}
	
	Pour commencer la composé de deux foncions $\mathscr{C}^1$ est $\mathscr{C}^1$. Ainsi, $g \circ f$ est $\mathscr{C}^1$. Soit ensuite $R > 0$. Il existe $K_R^f > 0$ tel que pour tous $x, y$ dans $B_E(0, R)$ on ai :
	$$ | \d f(x) - \d f(y) |_{\mathscr{L}(E, F)} \leqslant K_R^f | x - y |_E $$
	De plus on a par théorème des accroissements finis :
	$$ | f(x) - f(y) |_F \leqslant \left( \sup_{z \in B_E(0, R)} | \d f(z) |_{\mathscr{L}(E, F)} \right) | x - y |_E $$
	On utilise ensuite l'inégalité triangulaire pour avoir :
	$$ | f(x) - f(y) |_F \leqslant \left( | \d f(0) |_{\mathscr{L}(E, F)} + \sup_{z \in B_E(0, R)} | \d f(z) - \d f(0) |_{\mathscr{L}(E, F)} \right) | x - y |_E \leqslant \left( | \d f(0) |_{\mathscr{L}(E, F)} + K_R^f R \right) | x - y |_E $$
	On pose alors :
	$$ L_R^f = \left( | \d f(0) |_{\mathscr{L}(E, F)} + K_R^f R \right) \qquad \text{et} \qquad R' = |f(0)|_F + L_R^f R $$
	De tel sorte que $f(x)$ et $f(y)$ soient dans la boule $B_F(0, R')$. Il existe alors $K_{R'}^g$ tel que :
	$$ | \d g(f(x)) - \d g(f(y)) |_{\mathscr{L}(F, G)} \leqslant K_{R'}^g | f(x) - f(y) |_F \leqslant K_{R'}^g L_R^f | x - y |_E $$
	On utilise ensuite une inégalité triangulaire sur la norme de la différence de la différentielle de $g \circ f$ évalué en $x$ et en $y$ :
	$$ \begin{array}{lll}
		\left| \d \left( g \circ f \right)(x) -  \d \left( g \circ f \right)(y) \right|
		& \leqslant & \left| \d g(f(x)) \circ \d f(x) - \d g(f(x)) \circ \d f(y) \right| + \left| \d g(f(x)) \circ \d f(y) - \d g(f(y)) \circ \d f(y) \right| \\[2mm]
		& \leqslant & \left| \d g(f(x)) \right| \left| \d f(x) - \d f(y) \right| + \left| \d g(f(x)) - \d g(f(y)) \right| \left| \d f(y) \right| \\
		& \leqslant & L_{R'}^g K_R^f | x - y | + K_{R'}^g L_R^f | x - y | L_R^f
	\end{array} $$
	Où $L_{R'}^g$ est défini de la même manière que $L_R^f$, à savoir :
	$$ L_{R'}^g = | \d g(0) |_{\mathscr{L}(F, G)} + K_{R'}^g R' $$
	On conclue alors en posant :
	$$ K_R^{g \circ f} = L_{R'}^g K_R^f + K_{R'}^g \left( L_R^f \right)^2 $$
	afin d'avoir :
	$$ \left| \d \left( g \circ f \right)(x) -  \d \left( g \circ f \right)(y) \right|_{\mathscr{L}(E, G)} \leqslant K_R^{g \circ f} | x - y |_E $$
	\begin{center}
		\fbox{La composition $g \circ f$ appartient à $\mathscr{C}_{loc}^{1, lip}$}
	\end{center}

	\subsection*{\textsc{Exercice} V.2.}
	
	\paragraph{(1)}
	Le hamiltonien réduit est défini par :
	$$ H_r(q, p) = \frac{1}{2} \left( j(q, p) \mid K j(q, p) \right) $$
	Dans le cas des landmarks, nous avons aussi :
	$$ j(q, p) = \sum_{i=1}^k \delta_{x_i}^{p_i} $$
	Où les $p_i$ sont des vecteurs de $\R^d$ tel que $p(q) = \sum_{i=1}^k \langle p_i, x_i \rangle_{\R^d}$. De plus le noyau $K$ est défini tel que :
	$$ K \delta_{x_i}^{p_i} = K(\cdot, x_i) p_i $$
	On peut alors se lancer dans les calculs :
	$$ H_r(q, p) = \frac{1}{2} \left( \sum_{i=1}^k \delta_{x_i}^{p_i} \bigm| \sum_{i=1}^k K(\cdot, x_i) p_i \right) = \frac{1}{2} \sum_{i=1}^k \delta_{x_i}^{p_i} \left( \sum_{j=1}^k K(\cdot, x_j) p_j \right) = \frac{1}{2} \sum_{i,j=1}^k p_i^\trans K(x_i, x_j) p_j $$
	
	\paragraph{(2)}
	Si $V \overset{cont.}{\hookrightarrow} C_b^2(\R^d, \R^d)$, alors $\eta$ est dans $\mathscr{C}_{loc}^{1, lip}(\R^d \times \R^d, V^*)$ d'après la proposition V.2. Puis comme on a :
	$$ j(q, p) = \sum_{i=1}^k \eta(x_i, p_i) $$
	On en déduit que $j$ est dans $\mathscr{C}_{loc}^{1, lip}(\mathscr{B} \times \mathscr{B}^*, V^*)$. En effet en notant $\pi_i$ la projection, $\pi_i(q, p) = (x_i, p_i)$. On remarque que c'est une fonction linéaire et donc $\d \pi(q, p) = \pi$ pour tout couple $(q, p)$. On en déduit immédiatement que $\pi_i$ est dans $\mathscr{C}_{loc}^{1, lip}(\mathscr{B} \times \mathscr{B}^*, \R^d \times \R^d)$. On peut alors exprimé $j$ comme :
	$$ j = \sum_{i=1}^k \eta \circ p_i $$
	Or d'après la proposition V.1., la composition de deux fonctions $\mathscr{C}_{loc}^{1, lip}$ est $\mathscr{C}_{loc}^{1, lip}$. Puis on peut montrer que la somme de deux fonctions $\mathscr{C}_{loc}^{1, lip}$ ($f$ et $g$ de $E$ dans $F$) est $\mathscr{C}_{loc}^{1, lip}$. En effet la somme de deux fonctions $\mathscr{C}^1$ est $\mathscr{C}^1$. Et soit $R > 0$, on sait qu'il existe $K_R^f$ et $K_R^g$ tel que pour tout $x,y$ dans $B_E(0, R)$ on a :
	$$ | \d f(x) - \d f(y) | \leqslant K_R^f | x - y | \qquad \text{et} \qquad 	| \d g(x) - \d g(y) | \leqslant K_R^g | x - y | $$
	On a alors par inégalité triangulaire :
	$$ | \d (f+g)(x) - \d (f+g)(y) | \leqslant | \d f(x) - \d f(y) | + | \d g(x) - \d g(y) | \leqslant (K_R^f + K_R^g) | x - y | $$
	Et donc $f+g$ est $\mathscr{C}_{loc}^{1, lip}(E, F)$. Ce qui nous donne finalement $j \in \mathscr{C}_{loc}^{1, lip}(\mathscr{B} \times \mathscr{B}^*, V^*)$. D'après la fin de la première section du chapitre V, on en déduit que $H_r$ appartient à $\mathscr{C}_{loc}^{1, lip}(\mathscr{B} \times \mathscr{B}^*, \R)$.
	
	\paragraph{(3)}
	Par symétrie de $K$, on a :
	$$ \frac{d x_i}{d t} = \frac{\partial H_r}{\partial p_i} = \sum_{j=1}^k K(x_i, x_j) p_j $$
	$$ \frac{d p_i}{d t} = - \frac{\partial H_r}{\partial x_i} = - \sum_{j=1}^k \frac{\partial}{\partial x_i} \left[ p_i K(x_i, x_j) p_j \right] $$
	
	\subsection*{\textsc{Exercice} V.3.}
	On a :
	$$ \begin{array}{lll}
		\left| \left( \d j(q, p) - \d j(q', p') \right).(\delta p, \delta q) \right|
		& = & \displaystyle \sup_{|v|_V = 1} \left| \left( \left( \d j(q, p) - \d j(q', p') \right).(\delta p, \delta q) \mid v \right) \right| \\
		& = & \displaystyle \sup_{|v|_V = 1} \left| \left( \delta p \mid v \circ q - v \circ q' \right) + \left( p \mid \d v \circ q . \delta q \right) - \left( p' \mid \d v \circ q' . \delta q \right) \right| \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} \left| \left( \delta p \mid v \circ q - v \circ q' \right) \right| + \left| \left( p \mid \d v \circ q . \delta q \right) - \left( p' \mid \d v \circ q' . \delta q \right) \right| \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} | \delta p | | v \circ q - v \circ q' | + \left| \left( p - p' \mid \d v \circ q . \delta q \right) + \left( p' \mid \left( \d v \circ q - \d v \circ q' \right) . \delta q \right) \right| \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} |\delta p| | \d v |_\infty | q - q' | + \left| \left( p - p' \mid \d v \circ q . \delta q \right) \right| + \left| \left( p' \mid \left( \d v \circ q - \d v \circ q' \right) . \delta q \right) \right| \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} |\delta p| K | v |_V | q - q' | + |p - p'| |\d v|_\infty | \delta q | + |p'| | \d v \circ q - \d v \circ q' |_\infty | \delta q | \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} |\delta p| K | q - q' | + |p - p'| K |v|_V | \delta q | + |p'| | \d^2 v |_\infty | q - q'|  | \delta q | \\
		& \leqslant & \displaystyle \sup_{|v|_V = 1} |\delta p| K | q - q' | + |p - p'| K | \delta q | + |p'| K | v |_V | q - q'|  | \delta q | \\
		& \leqslant & \displaystyle K \left( (|\delta p| + |p'| | \delta q |) | q - q' | + | \delta q | |p - p'| \right) \\
		\end{array}
	$$
	D'où :
	$$ \left| \d j(q, p) - \d j(q', p') \right| \leqslant K \left( (1 + |p|) |q - q'| + |p - p'| \right) $$
	En prenant la constante $K_R = K(2 + R)$, on obtient que $\d j$ est localement Lipschitzienne. On conclue alors :
	\begin{center}
		\fbox{$j \in \mathscr{C}_{loc}^{1, lip}(\mathscr{B} \times B_e^*, V^*)$}
	\end{center}

	\subsection*{\textsc{Exercice} V.4.}
	On a :
	$$ g(q_1 + \delta q_1) = \frac{1}{2} \int_0^1 | q_1(s) + \delta q_1(s) - y(s) |^2 ds = g(q_1) + \int_0^1 \langle q_1(s) - y(s), \delta q_1(s) \rangle ds $$
	Donc $g$ est dérivable, de dérivée $g'(q_1) = q_1 - y \in \mathscr{C}([0, 1], \R^d)$. Cette dérivée étant affine, elle est continue.
	\begin{center}
		\fbox{$g$ est $\mathscr{C}^1$ sur $\mathscr{C}([0, 1], \R^d)$ à dérivée dans $\mathscr{C}([0, 1], \R^d)$.}
	\end{center}

	\subsection*{\textsc{Exercice} V.5.}
	On a :
	$$ \begin{array}{lll}
	H_r(q, p)
	& = & \displaystyle \frac{1}{2} \left( j(q, p) \mid K j(q, p) \right) \\[3mm]
	& = & \displaystyle \frac{1}{2}\left( p \mid K j(p, q) \circ q \right) \\[1mm]
	& = & \displaystyle \frac{1}{2} \int_0^1 p^\trans(r) \left( K j(p, q) \circ q(r) \right) dr \\
	& = & \displaystyle \frac{1}{2} \int_0^1 \int_0^1 p^\trans(r) K(q(r), q(s)) p(s) dr ds
	\end{array} $$
	Puis :
	$$ \dot{q}(r) = \frac{\partial H_r}{\partial p}(r) = \int_0^1 K(q(r), q(s)) p(s) ds $$
	$$ \dot{p}(r) = - \frac{\partial H_r}{\partial q}(r) = - \int_0^1 \frac{\partial}{\partial x} \left[ p(r) K(x, q(s)) p(s) \right](q(r)) ds $$
\end{document}