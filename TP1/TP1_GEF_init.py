#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 16:24:16 2018

@author: glaunes
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#%%###########
# question 2 #
#%%###########

# fonction pour définir le noyau 
def KH1(x,y,sigma=1):
	return np.exp(- abs(x - y) / sigma) / (2*sigma)

# fonction pour définir le produit scalaire
def H1ScalProd(f,g,sigma=1):
	# f et g sont deux fonctions python
	# doit renvoyer <f,g>_H1 calculé par sommes et différences finies
	x = np.linspace(-15*sigma, 15*sigma, 20000)
	dx = x[1:] - x[:-1]
	fx, gx = f(x), g(x)
	mfx, mgx = 0.5*(fx[1:] + fx[:-1]), 0.5*(gx[1:] + gx[:-1])
	dfx, dgx = fx[1:] - fx[:-1], gx[1:] - gx[:-1]
	return mfx.dot(mgx * dx) + sigma**2 * dfx.dot(dgx / dx)

if False:
	# définition des paramètres et fonctions à tester    
	sigma = 2
	x0 = 2
	f = lambda x: np.exp(-x**2)
	Kx = lambda y: KH1(y, x0, sigma)

	# test : valeur du produit scalaire <f,KH1(.,x)>_H1 et comparaison avec f(x)
	prod = H1ScalProd(f, Kx, sigma)
	fx = f(x0)
	print("====== Question 2 ======")
	print(f"x = {x0} \t sigma = {sigma}")
	print("<f,KH1(.,x)>_H1 :", prod)
	print("f(x) :", fx)
	print("erreur relative :", 2*abs(fx-prod)/(abs(fx) + abs(prod)))

	# vérification de la propriété reproduisante : calcul de <KH1(.,y),KH1(.,x)>_H1 et comparaison avec KH1(x,y)
	y0 = -1
	prod = H1ScalProd(Kx, lambda x: KH1(x, y0, sigma), sigma)
	Kxy = KH1(x0, y0, sigma)
	print()
	print(f"y = {y0}")
	print("<KH1(.,y),KH1(.,x)>_H1 :", prod)
	print("KH1(x,y) :", Kxy)
	print("erreur relative :", 2*abs(Kxy-prod)/(abs(Kxy) + abs(prod)))
	print("========================")

#%%###########
# question 3 #
#%%###########

def KernelMatrix(x,y,h):
	p, d = x.shape
	n, d2 = y.shape
	assert d == d2, (d, d2)
	return h(np.linalg.norm(x.reshape(p, 1, d) - y, axis=2))

def gauss(sigma):
	def f(u):
		return np.exp(-u**2/sigma**2)
	return f

if False:
	x = np.random.rand(10,2)    # 10 points tirés aléatoirement dans [0,1]^2
	y = np.random.rand(15,2)    # 15 points tirés aléatoirement dans [0,1]^2
	sigma = 1.5                 # échelle du noyau
	Kxy = KernelMatrix(x,y,gauss(sigma))
	print(Kxy)
  

#%%###########
# question 4 #
#%%###########

def Interp(x,y,c,h):
	# ... à compléter
	pass

if False:
	y = np.array([0,2,4,8])[:,None]     # vecteur [0,2,4,8] sous forme de tableau 4x1
	c = np.random.randn(4,1)            # valeurs tirées aléatoirement
	x = np.linspace(-2,10,1000)[:,None] # points xj où évaluer l'interpolation
	fx = Interp(x,y,c,gauss(sigma))     # calcul de l'interpolation
	plt.plot(y,c,'o')                   # affichage
	plt.plot(x,fx)

# tests avec différents sigma et différents noyaux 
# ... à compléter

#%%###########
# question 6 #
#%%###########
		
# ... à compléter

#%%###########
# question 7 #
#%%###########

def InterpGrid(X1,X2,y,c,h) :
	# ... à compléter
	# doit renvoyer l'évaluation de l'interpolant à noyau sur les points de la grille X1,X2
	pass

if False:
	n = 10
	d = 2
	sigma = .25
	y = np.random.rand(n,d)    # 10 points tirés aléatoirement dans [0,1]^2
	c = np.random.randn(n,1)   # 10 valeurs aléatoires
	t = np.linspace(0,1,50)
	X1, X2 = np.meshgrid(t,t)  # grille uniforme de 50*50 points
	Z = InterpGrid(X1,X2,y,c,gauss(sigma))
	# affichage :
	fig = plt.figure()
	plt.title("interpolation de fonction 2D")
	ax = plt.axes(projection='3d')
	ax.plot_surface(X1,X2,Z)
	ax.scatter3D(y[:,0],y[:,1],c,c='r',depthshade=False)


#%%###########
# question 8 #
#%%###########

# redéfinition de Interp pour inclure le paramètre lambda
def Interp(x,y,c,h,l=0):
	# ... à compléter
	pass

if False:
# tests pour différentes valeurs de sigma et de lambda
	n = 30
	y = np.linspace(0,1,n)[:,None]
	c = np.cos(6*y) + .05*np.random.randn(n,1)
	# ... à compléter


#%%###########
# question 9 #
#%%###########

# ... à compléter si besoin


#%%############
# question 10 #
#%%############

def InterpGrid2D(X1,X2,y,c,h,l=0) :
	# ... à compléter
	# modification de InterpGrid devant renvoyer un interpolant vectoriel et non scalaire
	pass

if False:	
	n = 3
	d = 2
	m = 2
	sigma = .5
	y = np.random.rand(n,d)
	gamma = 0.05*np.random.randn(n,m)
	t = np.linspace(0,1,20)
	X1, X2 = np.meshgrid(t,t)
	V1, V2 = InterpGrid2D(X1,X2,y,gamma,gauss(sigma))
	plt.figure();
	plt.title("interpolation vectorielle, "+id+", sigma="+str(sigma))
	plt.quiver(X1,X2,V1,V2,color='b');
	plt.quiver(y[:,0],y[:,1],gamma[:,0],gamma[:,1]);


#%%############
# question 11 #
#%%############

# ... à compléter

#%%############
# question 12 #
#%%############

def save(obj,fname='store.pckl'):
	import pickle
	f = open(fname, 'wb')
	pickle.dump(obj, f)
	f.close()
def load(fname='store.pckl'):
	import pickle
	f = open(fname, 'rb')
	obj = pickle.load(f)
	f.close()
	return obj
	
fname = "hands.pckl"
if False:
	import scipy.io
	C1 = scipy.io.loadmat('hand1.mat')['C1']
	C2 = scipy.io.loadmat('hand2.mat')['C2']
	plt.figure()
	plt.plot(C1[:,0],C1[:,1])
	plt.title("Selection des landmarks - courbe 1")
	lmk1 = np.array(plt.ginput(0))
	plt.figure()
	plt.plot(C2[:,0],C2[:,1])
	plt.title("Selection des landmarks - courbe 2")
	lmk2 = np.array(plt.ginput(0))
	save((C1,C2,lmk1,lmk2),fname)
else:
	C1,C2,lmk1,lmk2 = load(fname)
	print(lmk1)
	print(lmk2)
	plt.plot(C1[:,0],C1[:,1])
	plt.plot(C2[:,0],C2[:,1])
	plt.show()

# ... à compléter : calcul de l'appariement et affichage 


#%%############
# question 13 #
#%%############

# ... à compléter


#%%############
# question 14 #
#%%############

# ... à compléter




