\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm, bm}
\usepackage{hyperref}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Géométrie et espaces de formes --- TP 1 \\
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\begin{document}
	
	\maketitle
	
	\section{Noyaux reproduisant et interpolation}
	
	\subsection*{Un premier exemple : espace de Sobolev $H^1(\R)$}
	
	\paragraph{1.}
	Soit $x \in \R$. On remarque que $K_\sigma(\cdot, x)$ est dérivable sur $\R \setminus \{x\}$, de dérivée :
	$$ K_\sigma(\cdot, x)'(y) = \syst{ll}{
		\frac{1}{\sigma} K_\sigma(y, x) = \frac{1}{2 \sigma^2} e^{- \frac{|x - y|}{\sigma}} & \text{Si } y < x \\
		- \frac{1}{\sigma} K_\sigma(y, x) = - \frac{1}{2 \sigma^2} e^{- \frac{|x - y|}{\sigma}} & \text{Si } y > x
	} $$
	Comme $y \mapsto e^{-|y|}$ est de carré intégrable, $K_\sigma(\cdot, x)$ et sa dérivée sont dans $L^2(\R)$. Ainsi les fonctions $K_\sigma(\cdot, x)$ appartiennent à $H_\sigma^1$. Soit alors $f \in H_\sigma^1$, calculons le produit scalaire entre $f$ et $K_\sigma(\cdot, x)$ :
	$$ \begin{array}{lll}
	\left\langle K_\sigma(\cdot, x), f \right\rangle_{H_\sigma^1}
	& = & \displaystyle \int_{-\infty}^x \left[ f(y) \cdot K_\sigma(y, x) + \sigma f'(y) \cdot K_\sigma(y, x) \right] dy - \int_x^{+\infty} \left[ f(y) \cdot K_\sigma(y, x) + \sigma f'(y) \cdot K_\sigma(y, x) \right] dy \\[5mm]
	& = & \displaystyle \left[ \sigma f(y) \cdot K_\sigma(y, x) \right]_{-\infty}^x - \left[ \sigma f(y) \cdot K_\sigma(y, x) \right]_x^{+\infty} \\[3mm]
	& = & \displaystyle \left( f(x) \cdot \frac{1}{2} - 0 \right) - \left( 0 - f(x) \cdot \frac{1}{2} \right) \\[2mm]
	& = & f(x)
	\end{array} $$
	\begin{center}
		\fbox{On en déduit que $H_\sigma^1$ est un espace à noyau reproduisant donc le noyau est $K_\sigma$.}
	\end{center}

	\paragraph{2.}
	Voir notebook.
	
	\subsection*{Problème d’interpolation scalaire}
	
	Voir notebook.
	
	\subsection*{Interpolation relaxée}
	
	\paragraph{8.}
	On note $E$ l'énergie à minimiser. En tant que fonction convexe et différentiable il suffit d'annuler son gradient pour trouver une solution. On rappelle que l'évaluation en un point s'écrit :
	$$ f(y_i) = \langle f, K_H(\cdot, y_i) \rangle_H $$
	Cela nous permet de calculer le gradient de $E$ :
	$$ \nabla E(f) = 2 \lambda f + 2 \sum_{i=1}^n K_H(\cdot, y_i) (f(y_i) - c_i) $$
	D'où :
	$$ f = \frac{1}{\lambda} \sum (c_i - f(y_i)) K_H(\cdot, y_i) $$
	La solution est alors toujours solution d'un système linéaire similaire. On cherche un vecteur $a$ qui vérifie :
	$$ a_i = \frac{1}{\lambda} (c_i - f(y_i)) = \frac{1}{\lambda} \left( c_i - \sum_{j=1}^n a_j K_H(y_i, y_j) \right) $$
	Sous forme matricielle on réécrit cette équation :
	\begin{center}
		\fbox{$\left( \lambda I_n + K_H(\bm{y}, \bm{y}) \right) \bm{a} = \bm{c}$.}
	\end{center}

	\subsection*{Problème d’interpolation vectorielle}
	
	\paragraph{9.}
	Commençons par le problème (3). On écrit le Lagrangien du problème en remarquant que minimiser la norme revient à minimiser la norme au carré :
	$$ L(v, \alpha) = \| v \|^2 + \sum_{i=1}^n \alpha_i^\trans (v(y_i) - \gamma_i) $$
	On annule alors le gradient du Lagrangien en $v$ :
	$$ \nabla_v E(v, \alpha) = 2v + \sum_{i=1}^n K_V(\cdot, y_i) \alpha_i = 0 $$
	Il existe alors des vecteurs $a_i = - \alpha_i/2$ tel que :
	$$ v = \sum_{i=1}^n K_V(\cdot, y_i) a_i $$
	On reprend alors la contrainte du problème qui nous donne :
	\begin{equation}
		\label{vecsys}
		\forall i = 1, ..., n \quad v(y_i) = \sum_{j=1}^n K_V(y_i, y_j) a_j = \gamma_i
	\end{equation}
	On rappelle l'expression du noyau :
	$$ K_V(x, y) = h(\|x - y\|) I_m $$
	Ainsi pour $i = 1, ... m$, en notant $a_j[i]$ la $i$-ème composante du vecteur $a_j$, on pose les vecteurs :
	$$ \bm{a[i]} = \pmat{a_1[i] \\ \vdots \\ a_n[i]} \qquad \bm{\gamma[i]} = \pmat{\gamma_1[i] \\ \vdots \\ \gamma_n[i]} $$
	Le système \eqref{vecsys} se transforme alors en $m$ problèmes d'interpolation scalaire :
	$$ \forall i = 1, ..., m \quad K_H(\bm{y}, \bm{y}) \bm{a[i]} = \bm{\gamma[i]} $$
	
	\paragraph{}
	On passe maintenant au problème (4). En notant $E$ l'énergie convexe à minimiser, on calcule son gradient qui s'annule en la solution :
	$$ \frac{1}{2} \nabla_v E(v) = \gamma v + \sum_{i=1}^n K_V(\cdot, y_i) \left( v(y_i) - \gamma_i \right) = \gamma v + \sum_{i=1}^n K_H(\cdot, y_i) (v(y_i) - \gamma_i) $$
	Ce problème se transforme donc en $m$ problèmes d'interpolations scalaire (un problème pour chaque coordonnées) :
	$$ \forall i = 1, ..., m \quad \left( \gamma I_n + K_H(\bm{y}, \bm{y}) \right) \bm{a[i]} = \bm{\gamma[i]} $$
	
	\section{Appariement de points labellisés}
	
	\subsection*{Appariement linéaire}
	
	Voir notebook
	
	\subsection*{Ajout des déplacements affines et modèle Thin Plate Splines}
	
	\paragraph{13.}
	Il s'agit de trouver $v$, $B$ et $c$. Puis à partir de ces derniers paramètres, la fonction $\phi$ est entièrement déterminé. Fixons $B$ et $c$. Dans ce cas on pose le problème devient :
	$$ \syst{lr}{
		v(y_i) = \gamma'_i = \gamma_i - B y_i - c & \forall i \in \{1, ..., n \} \\
		\| v \|_V \text{ minimal}
	} $$
	Où $\gamma_i = z_i - y_i$. On a vu précédemment que la solution de ce problème est :
	$$ v = \sum_{i=1}^n K_V(\cdot, y_i) a_i \qquad \text{avec} \quad \forall j = 1, ..., m; \; K_H(\bm{y}, \bm{y}) \bm{a[j]} = \bm{\gamma'[j]} = \pmat{\left( \gamma_1 - By_1 - c \right)[j] \\ \vdots \\ \left( \gamma_n - By_n - c \right)[j]} $$
	La norme de $v$ vaut alors :
	$$ \| v \|_V^2 = \sum_{1 \leqslant i, k \leqslant n} a_i^\trans K_V(y_i, y_k) a_k = \sum_{j=1}^m \bm{a[j]}^\trans K_H(\bm{y}, \bm{y}) \bm{a[j]} = \sum_{j=1}^m \bm{\gamma'[j]}^\trans K_H(\bm{y}, \bm{y})^{-1} \bm{\gamma'[j]} $$
	Pour simplifier, dans la suite on notera $\bm{K}$ la matrice $K_H(\bm{y}, \bm{y})$. Soit $j \in \{1, ..., m\}$, on a :
	$$ \bm{\gamma'[j]}^\trans K_H(\bm{y}, \bm{y})^{-1} \bm{\gamma'[j]} = \sum_{1 \leqslant i, k \leqslant n} \left( \gamma_i - By_i - c \right)[j] \bm{K}_{ik}^{-1} \left( \gamma_k - By_k - c \right)[j] $$
	On veut donc minimiser :
	$$ E(B, c) = \sum_{1 \leqslant i, k \leqslant n} y_i^\trans B^\trans \bm{K}_{ik}^{-1} B y_k + c^\trans \bm{K}_{ik}^{-1} c + 2 c^\trans \bm{K}_{ik}^{-1} B y_k - 2 \gamma_i^\trans \bm{K}_{ik}^{-1} B y_k - 2 \gamma_i^\trans \bm{K}_{ik}^{-1} c $$
	Par positivité de $\bm{K}$, cette fonction est convexe. Notre solution est atteinte en un point d'annulation du gradient de $E$. Calculons ce dernier :
	$$ \nabla_c E(B, c) = 2 \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \left( c + B y_k - \gamma_k \right) $$
	$$ \nabla_B E(B, c) = 2 \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \left( c + B y_k - \gamma_k \right) y_i^\trans $$
	Ainsi la solution $B, c$ vérifie le système suivant :
	\begin{center}
		\fbox{$\syst{lll}{
			\displaystyle \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \left( c + By_k \right) & = & \displaystyle \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \gamma_k \\[5mm]
			\displaystyle \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \left( c + By_k \right) y_i^\trans & = & \displaystyle \sum_{1 \leqslant i, k \leqslant n} \bm{K}_{ik}^{-1} \gamma_k y_i^\trans
		}$}
	\end{center}

	\paragraph{14.}
	Voir notebook

\end{document}